#include <stdlib.h>
#include "stack.h"

void search_width(node graph[], int *contiguity, int n){
	int i;
	Node_t *head = NULL;
	graph[n].value=0;//присвоили ей значение начальной глубины
	graph[n].flag='y';//помечаем вершину как просмотренную
	push_stack(&head, graph[n]);
	while(head!=NULL){//пока стек не пустой счетаем
		n = peek(head).number;//берём номер верхней вершины в стеке
		for(i = 0; i<NUMBER_OF_NODES; i++){
			if (*(contiguity+NUMBER_OF_NODES*n+i)==1 && 
			graph[i].flag=='n'){
				graph[i].flag='y';//помечаем вершину как просмотренную
				graph[i].value=graph[n].value+1;//записали глубину
				push_queue(head, graph[i]);//погрузили в стек
			}
		}
		pop(&head);//убираем вершину из стека
	}
}
