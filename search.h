#define NUMBER_OF_NODES 7

typedef struct Node{
	int number;
	int value;
	char flag;
} node;

void search_depth(node *, int *, int);
void search_width(node *, int *, int);
