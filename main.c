#include <stdio.h>
#include <dlfcn.h>
#include "search.h"

int main(int argc, char* argv[]){
	
	int search_mode = 0;
	int n_start;
	node graph[NUMBER_OF_NODES];
	int i;
	for(i=0; i<NUMBER_OF_NODES; i++){
		graph[i].value = 0;
		graph[i].flag = 'n';
		graph[i].number = i;
	}
	
	int contiguity[NUMBER_OF_NODES][NUMBER_OF_NODES] = { 
		{ 0, 1, 1, 0, 0, 0, 0 },
		{ 1, 0, 0, 1, 1, 1, 0 },
		{ 1, 0, 0, 0, 0, 0, 1 },
		{ 0, 1, 0, 0, 1, 0, 0 },
		{ 0, 1, 0, 1, 0, 0, 0 },
		{ 0, 1, 0, 0, 0, 0, 0 },
		{ 0, 0, 1, 0, 0, 0, 0 } 
	};
	
	printf("С какой вершины считать? (1..%d)\n", NUMBER_OF_NODES);
	scanf("%d", &n_start);
	n_start--;
	printf("Выбирете режим поиска:\n1.Поиск в глубину\n2.\
Поиск в ширину\n(1 или 2)\n");
	scanf("%d", &search_mode);
	switch(search_mode)
	{
		case 1: 
			//depth search in tree
			search_depth(graph, (int *)contiguity, n_start);
			break;
		case 2: 
			//wide-width search in tree
			search_width(graph, (int *)contiguity, n_start);
			break;
		default: 
			printf("\nТакого режима нет!\n");
			break;
	}
	printf("Пройденные расcтояния до вершин:\n");
	for(i=0;i<NUMBER_OF_NODES;i++){
		printf("%d: %d\n", graph[i].number+1, graph[i].value);
	}
}
