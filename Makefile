#This is a Make_file_SearchInTree  

SearchInTree: main.o search_depth.o search_width.o stack.o
	gcc -o SearchInTree main.o search_depth.o search_width.o stack.o
	rm -f *.o
main.o: main.c
	gcc main.c -c
stack.o: stack.c
	gcc stack.c -c
search_depth.o: search_depth.c
	gcc search_depth.c -c
search_width.o: search_width.c
	gcc search_width.c -c
clean:
	rm -f SearchInTree *.o *.so *.gch
