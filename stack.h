#include "search.h"
#define STACK_OVERFLOW  -100
#define STACK_UNDERFLOW -101
#define OUT_OF_MEMORY   -102

typedef struct Node T; 
typedef struct Node_tag {
    T value;
    struct Node_tag *next;
} Node_t;

void push_stack(Node_t **, T);
void pop(Node_t **);
T peek(const Node_t *);
int getSize(const Node_t *);
void push_queue(Node_t *, T);
//void printStack(const Node_t *);
