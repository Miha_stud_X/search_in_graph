#include <stdlib.h>
#include "stack.h"

void search_depth(node *graph, int *contiguity, int n){
	int i, deep = 0;
	Node_t *head = NULL;
	graph[n].value=deep;//присвоили ей значение начальной глубины
	graph[n].flag='y';//помечаем вершину как просмотренную
	push_stack(&head, graph[n]);
	while(head!=NULL){//пока стек не пустой счетаем
		n = peek(head).number;//берём номер верхней вершины в стеке
		for(i = 0; i<NUMBER_OF_NODES; i++){
			if (*(contiguity+NUMBER_OF_NODES*n+i)==1 && 
			graph[i].flag=='n'){
				graph[i].flag='y';//помечаем вершину как просмотренную
				deep++;//увеличили глубину	
				push_stack(&head, graph[i]);//погрузили в стек
				break;
			}
		}
		if(i == NUMBER_OF_NODES){
			//если в узле не осталось непросмотренных потомоков, то
			graph[n].value=deep--;//присвоили и изменили глубину
			pop(&head);//убираем вершину из стека
		}
	}
}
