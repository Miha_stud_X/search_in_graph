#include <stdlib.h>
#include "stack.h"

void push_stack(Node_t **head, T value){
    Node_t *tmp = malloc(sizeof(Node_t));
	if (tmp == NULL) {
        exit(STACK_OVERFLOW);
    }
    tmp->next = *head;
    tmp->value = value;
    *head = tmp;
}

void push_queue(Node_t *head, T value){
    Node_t *tmp = malloc(sizeof(Node_t));
	if (tmp == NULL) {
        exit(STACK_OVERFLOW);
    }
    tmp->next = NULL;
    tmp->value = value;
    while(head->next != NULL){
		head = head->next;
	}
    head->next = tmp;
}

void pop(Node_t **head){
    if ((*head) == NULL) {
        exit(STACK_UNDERFLOW);
    }
    *head = (*head)->next;
}

T peek(const Node_t* head){
    if (head == NULL) {
        exit(STACK_UNDERFLOW);
    }
    return head->value;
}

int getSize(const Node_t *head){
    int size = 0;
    while (head) {
        size++;
        head = head->next;
    }
    return size;
}

/*
void printStack(const Node_t* head){
    printf("Стек -> ");
    while (head) {
        printf("%d ", head->value);
        head = head->next;
    }
}*/

